public class SubtractonacciSequence extends Sequence{
    public int n1;
    public int n2;

    public static void main(String[] args) {
        SubtractonacciSequence s = new SubtractonacciSequence(2, 4);
        System.out.println(s.getTerm(5));
    }

    public SubtractonacciSequence(int n1, int n2) {
        this.n1 = n1;
        this.n2 = n2;
        
    }

    public int getTerm(int n) {
        int num1 = this.n1;
        int num2 = this.n2;
        if (n > 2) {
        int num3 = 0;
        for(int i = 2; i < n; i++) {
           num3 = num1 - num2;
           num1 = num2;
           num2 = num3;
        }
        return num3; 
    } else if (n == 1) {
        return num1;
    } else return num2;
    } 
}