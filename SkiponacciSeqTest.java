import org.junit.Test;
import static org.junit.jupiter.api.Assertions.*;

public class SkiponacciSeqTest {
    
    @Test
    public void calculateSequenceTest() {
        SkiponacciSequence test = new SkiponacciSequence(2, 4, 1);
        assertEquals(test.getTerm(4), 6);

    }

    @Test
    public void calculateSequenceTest2() {
        SkiponacciSequence test = new SkiponacciSequence(2, 4, 1);
        assertEquals(test.getTerm(9), 18); 

    }

    @Test
    public void calculateSequenceTest3() {
        SkiponacciSequence test = new SkiponacciSequence(2, 4, 1);
        assertEquals(test.getTerm(1), 2); // Test for the first term in the sequence

    }

    @Test
    public void calculateSequenceTest4() {
        SkiponacciSequence test = new SkiponacciSequence(2, 4, 1);
        assertEquals(test.getTerm(2),4); // Test for the second term in the sequence

    }
}
