import org.junit.Test;
import static org.junit.jupiter.api.Assertions.*;


public class TestSubtractionacci {
    

    @Test
    public void testGetTerm() {
        SubtractonacciSequence sub = new SubtractonacciSequence(2, 4);
        assertEquals(sub.getTerm(3), -2);
    }

    @Test
    public void testGetTerm2() {
        SubtractonacciSequence sub = new SubtractonacciSequence(2, 4);
        assertEquals(sub.getTerm(7), -22);
    }

    @Test
    public void testgetFirstTerm() {
        SubtractonacciSequence sub = new SubtractonacciSequence(2, 4);
        assertEquals(sub.getTerm(1), 2);
    }

    @Test

    public void testSameTerm() {
        SubtractonacciSequence sub = new SubtractonacciSequence(2, 4);
        assertEquals(sub.getTerm(4), sub.getTerm(4));
    }

}

