public class SkiponacciSequence extends Sequence {
    private int a;
    private int b;
    private int c;

    public SkiponacciSequence(int a, int b, int c) {
        this.a = a;
        this.b = b;
        this.c = c;

    }

    public int getTerm(int n) {
        int[] term;
        if (n < 3) {
            term = new int[3];
        } else {
            term = new int[n];
        }
        
        term[0] = this.a;
        term[1] = this.b;
        term[2] = this.c;

        if (n > 3) {
            for (int i = 3; i < n; i++) {
                term[i] = term[i-2] + term[i-3];
            }
        }
        return term[n-1];
    }


}