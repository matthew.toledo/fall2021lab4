import java.util.Scanner;
public class Application {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        System.out.println("Enter a String of sequences");
        String s = keyboard.next();
        Sequence[] seqArr = Application.parse(s);

        for (Sequence seq : seqArr) {
            for (int i = 1; i <11; i++) {
                System.out.print(seq.getTerm(i) + " ");
                
            }
            System.out.println();System.out.println();
        }


    }

    public static void print(Sequence[] seq, int n) {
        for ( int i = 0; i < seq.length; i++) {

            for( int j = 0; j < n; j++) {
                System.out.println(seq[j].getTerm(n));
            }
        }
    }

    public static Sequence[] parse(String s) {
        String[] parts = s.split(";");
        Sequence[] seqArr = new Sequence[parts.length/4];
        int sequenceCounter = 0;
        for (int i = 0; i < parts.length; i+=4) {
            if (parts[i].equals("Skip")) {
                seqArr[sequenceCounter] = new SkiponacciSequence(i+1, i+2, i+3);
                sequenceCounter++;
            } else if (parts[i].equals("Sub")) {
                seqArr[sequenceCounter] = new SubtractonacciSequence(i+1, i+2);
                sequenceCounter++;
            }
        } return seqArr;

    }
}
